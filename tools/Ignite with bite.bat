@echo off
color a
setlocal EnableDelayedExpansion
set /p in=Do you really want to execute the script? [y/n]
if "!in!"=="y" goto execscript
if "!in!"=="j" goto execscript
if "!in!"=="" goto execscript
goto end
:execscript
cls
if NOT EXIST "%CD%\iteration1part1.txt" (
  echo. > "%CD%\iteration1part1.txt"
  echo First remove everything you can!
  pause
  cls
  echo Getting install_wim_tweak...
    PowerShell -Command "& {Invoke-WebRequest https://raw.githubusercontent.com/adolfintel/Windows10-Privacy/master/data/install_wim_tweak.zip -OutFile '%CD%\install_wim_tweak.zip'}"
    PowerShell -Command "& {Expand-Archive -Force '%CD%\install_wim_tweak.zip' 'C:\Windows\System32'}"
    del "%CD%\install_wim_tweak.zip"
  echo Go to C:\Windows\System32, check for install_wim_tweak and continue
  pause
  cls
  echo Getting the Ninite Apps...
    PowerShell -Command "& {Invoke-WebRequest https://ninite.com/firefox-irfanview-klitecodecs-sumatrapdf-winrar/ninite.exe -OutFile '%CD%\Ninite.exe'}"
    "%CD%\Ninite.exe"
  pause
    del "%CD%\Ninite.exe"
  cls
  echo Removing Windows Defender...
    reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v SmartScreenEnabled /t REG_SZ /d "Off" /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d "0" /f
    reg add "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d "0" /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender" /v DisableAntiSpyware /t REG_DWORD /d 1 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v SpyNetReporting /t REG_DWORD /d 0 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v SubmitSamplesConsent /t REG_DWORD /d 2 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v DontReportInfectionInformation /t REG_DWORD /d 1 /f
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\Sense" /f
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\SecurityHealthService" /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontReportInfectionInformation" /t REG_DWORD /d 1 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontOfferThroughWUAU" /t REG_DWORD /d 1 /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "SecurityHealth" /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run" /v "SecurityHealth" /f
    reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\SecHealthUI.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
    install_wim_tweak /o /c Windows-Defender /r
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Notifications\Settings\Windows.SystemToast.SecurityAndMaintenance" /v "Enabled" /t REG_DWORD /d 0 /f
  echo Done!
  cls
  echo Removing Windows Store...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *store* | Remove-AppxPackage}"
    install_wim_tweak /o /c Microsoft-Windows-ContentDeliveryManager /r
    install_wim_tweak /o /c Microsoft-Windows-Store /r
    reg add "HKLM\Software\Policies\Microsoft\WindowsStore" /v RemoveWindowsStore /t REG_DWORD /d 1 /f
    reg add "HKLM\Software\Policies\Microsoft\WindowsStore" /v DisableStoreApps /t REG_DWORD /d 1 /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d 0 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\PushToInstall" /v DisablePushToInstall /t REG_DWORD /d 1 /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v SilentInstalledAppsEnabled /t REG_DWORD /d 0 /f
    sc delete PushToInstall
  echo Done!
  cls
  echo Removing Music, TV, etc. ...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *zune* | Remove-AppxPackage}"
    PowerShell -Command "& {Get-WindowsPackage -Online | Where PackageName -like *MediaPlayer* | Remove-WindowsPackage -Online -NoRestart}"
  echo Done!
  cls
  echo Removing XBox and Game DVR...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *xbox* | Remove-AppxPackage}"
    sc delete XblAuthManager
    sc delete XblGameSave
    sc delete XboxNetApiSvc
    sc delete XboxGipSvc
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\xbgm" /f
    schtasks /Change /TN "Microsoft\XblGameSave\XblGameSaveTask" /disable
    schtasks /Change /TN "Microsoft\XblGameSave\XblGameSaveTaskLogon" /disable
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\GameDVR" /v AllowGameDVR /t REG_DWORD /d 0 /f
  echo Done!
  cls
  echo Removing Sticky Notes...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *sticky* | Remove-AppxPackage}"
  echo Done!
  cls
  echo Removing Maps...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *maps* | Remove-AppxPackage}"
    sc delete MapsBroker
    sc delete lfsvc
    schtasks /Change /TN "\Microsoft\Windows\Maps\MapsUpdateTask" /disable
    schtasks /Change /TN "\Microsoft\Windows\Maps\MapsToastTask" /disable
  echo Done!
  cls
  echo Removing Alarms and Clock...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *alarms* | Remove-AppxPackage}"
    PowerShell -Command "& {Get-AppxPackage -AllUsers *people* | Remove-AppxPackage}"
  echo Done!
  cls
  echo Removing Mail, Calendar, etc. ...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *comm* | Remove-AppxPackage}"
    PowerShell -Command "& {Get-AppxPackage -AllUsers *mess* | Remove-AppxPackage}"
  echo Done!
  cls
  echo Removing OneNote...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *onenote* | Remove-AppxPackage}"
  echo Done!
  cls
  echo Removing Photos...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *photo* | Remove-AppxPackage}"
  echo Done!
  cls
)
if EXIST "%CD%\iteration1part1.txt" (
  if NOT EXIST "%CD%\iteration1part2.txt" (
    echo. > "%CD%\iteration1part2.txt"
    echo Removing Camera...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *camera* | Remove-AppxPackage}"
    echo Done!
    cls
    echo Removing Weather, News, etc. ...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *bing* | Remove-AppxPackage}"
    echo Done!
    cls
    echo Removing Calculator...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *calc* | Remove-AppxPackage}"
    echo Done!
    cls
    echo Removing Sound Recorder...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *soundrec* | Remove-AppxPackage}"
    echo Done!
    cls
    echo Removing Contact Support and Get Help
      install_wim_tweak /o /c Microsoft-Windows-ContactSupport /r
      PowerShell -Command "& {Get-AppxPackage -AllUsers *GetHelp* | Remove-AppxPackage}"
    echo Additionally, go to Start > Settings > Apps > Manage optional features, and remove Contact Support, if present!
    cls
    echo Removing Microsoft Quick Assist...
      PowerShell -Command "& {Get-WindowsPackage -Online | Where PackageName -like *QuickAssist* | Remove-WindowsPackage -Online -NoRestart}"
    echo Done!
    cls
    echo Removing Connect...
      install_wim_tweak /o /c Microsoft-PPIProjection-Package /r
    echo Done!
    cls
    echo Removing Your Phone...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *phone* | Remove-AppxPackage}"
    echo Done!
    cls
    echo Removing Hello Face...
      PowerShell -Command "& {Get-WindowsPackage -Online | Where PackageName -like *Hello-Face* | Remove-WindowsPackage -Online -NoRestart}"
      schtasks /Change /TN "\Microsoft\Windows\HelloFace\FODCleanupTask" /Disable
    echo Done!
    cls
    echo Removing Paint 3D and Print 3D...
      for /f "tokens=1* delims=" %%I in (' reg query "HKEY_CLASSES_ROOT\SystemFileAssociations" /s /k /f "3D Edit" ^| find /i "3D Edit" ') do (reg delete "%%I" /f )
      for /f "tokens=1* delims=" %%I in (' reg query "HKEY_CLASSES_ROOT\SystemFileAssociations" /s /k /f "3D Print" ^| find /i "3D Print" ') do (reg delete "%%I" /f )
    echo Done!
    cls
    echo Removing System Restore...
      PowerShell -Command "& {Disable-ComputerRestore -Drive 'C:\'}"
      PowerShell -Command "& {vssadmin delete shadows /all /Quiet}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore' /v 'DisableConfig' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore' /v 'DisableSR ' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore' /v 'DisableConfig' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore' /v 'DisableSR ' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {schtasks /Change /TN '\Microsoft\Windows\SystemRestore\SR' /disable}"
    echo Done!
    cls
    echo Your system will reboot now!
    shutdown -r -t 1
    goto end
  )
)
if EXIST "%CD%\iteration1part1.txt" (
  if EXIST "%CD%\iteration1part2.txt" (
    if NOT EXIST "%CD%\iteration2.txt" (
      echo. > "%CD%\iteration2.txt"
      echo Disabling Cortana...
        reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v AllowCortana /t REG_DWORD /d 0 /f
        reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules"  /v "{2765E0F4-2918-4A46-B9C9-43CDD8FCBA2B}" /t REG_SZ /d  "BlockCortana|Action=Block|Active=TRUE|Dir=Out|App=C:\windows\systemapps\microsoft.windows.cortana_cw5n1h2txyewy\searchui.exe|Name=Search  and Cortana  application|AppPkgId=S-1-15-2-1861897761-1695161497-2927542615-642690995-327840285-2659745135-2630312742|" /f
      echo Done!
      cls
      echo Your system will reboot now!
      shutdown -r -t 1
      goto end
    )
  )
)
if EXIST "%CD%\iteration1part1.txt" (
  if EXIST "%CD%\iteration1part2.txt" (
    if EXIST "%CD%\iteration2.txt" (
      if NOT EXIST "%CD%\iteration3.txt" (
        echo. > "%CD%\iteration3.txt"
        echo Disabling Windows Error Report...
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f
        echo Done!
        cls
        echo Disabling Forced Updates...
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v NoAutoUpdate /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v AUOptions /t REG_DWORD /d 2 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v ScheduledInstallDay /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v ScheduledInstallTime /t REG_DWORD /d 3 /f
        echo Done!
        cls
        echo Disabling License Checking...
          reg add "HKLM\Software\Policies\Microsoft\Windows NT\CurrentVersion\Software Protection Platform" /v NoGenTicket /t REG_DWORD /d 1 /f
        echo Done!
        cls
        echo Diabling Sync...
          reg add "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v DisableSettingSync /t REG_DWORD /d 2 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v DisableSettingSyncUserOverride /t REG_DWORD /d 1 /f
        echo Done!
        cls
        echo Disabling Windows Tips...
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableSoftLanding /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsSpotlightFeatures /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsConsumerFeatures /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\DataCollection" /v DoNotShowFeedbackNotifications /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\WindowsInkWorkspace" /v AllowSuggestedAppsInWindowsInkWorkspace /t REG_DWORD /d 0 /f
        echo Done!
        cls
        echo Removing Telemetry and other Unnecessary Services...
          sc delete DiagTrack
          sc delete dmwappushservice
          sc delete WerSvc
          sc delete OneSyncSvc
          sc delete MessagingService
          sc delete wercplsupport
          sc delete PcaSvc
          sc config wlidsvc start=demand
          sc delete wisvc
          sc delete RetailDemo
          sc delete diagsvc
          sc delete shpamsvc 
          sc delete TermService
          sc delete UmRdpService
          sc delete SessionEnv
          sc delete TroubleshootingSvc
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "wscsvc" ^| find /i "wscsvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "OneSyncSvc" ^| find /i "OneSyncSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "MessagingService" ^| find /i "MessagingService"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "PimIndexMaintenanceSvc" ^| find /i "PimIndexMaintenanceSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "UserDataSvc" ^| find /i "UserDataSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "UnistoreSvc" ^| find /i "UnistoreSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "BcastDVRUserService" ^| find /i "BcastDVRUserService"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "Sgrmbroker" ^| find /i "Sgrmbroker"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "ClipSVC" ^| find /i "ClipSVC"') do (reg delete %%I /f)
          sc delete diagnosticshub.standardcollector.service
          reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules" /v "NumberOfSIUFInPeriod" /t REG_DWORD /d 0 /f
          reg delete "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules" /v "PeriodInNanoSeconds" /f
          reg add "HKLM\SYSTEM\ControlSet001\Control\WMI\AutoLogger\AutoLogger-Diagtrack-Listener" /v Start /t REG_DWORD /d 0 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v AITEnable /t REG_DWORD /d 0 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisableInventory /t REG_DWORD /d 1 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisablePCA /t REG_DWORD /d 1 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisableUAR /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableSmartScreen" /t REG_DWORD /d 0 /f
          reg add "HKCU\Software\Microsoft\Internet Explorer\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d 0 /f
          reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoRecentDocsHistory" /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\CompatTelRunner.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\DeviceCensus.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
        echo Done!
        cls
        start firefox.exe -private-window https://github.com/adolfintel/Windows10-Privacy
        echo Go to https://github.com/adolfintel/Windows10-Privacy and follow the steps to delete the keys DPS, WdiServiceHost and WdiServiceHost from the registry!
        pause
        cls
        echo Disabling Scheduled Tasks...
          schtasks /Change /TN "Microsoft\Windows\AppID\SmartScreenSpecific" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\AitAgent" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\ProgramDataUpdater" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\StartupAppTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Autochk\Proxy" /disable
          schtasks /Change /TN "Microsoft\Windows\CloudExperienceHost\CreateObjectTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\BthSQM" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Uploader" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /disable
          schtasks /Change /TN "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" /disable
          schtasks /Change /TN "Microsoft\Windows\DiskFootprint\Diagnostics" /disable
          schtasks /Change /TN "Microsoft\Windows\FileHistory\File History (maintenance mode)" /disable
          schtasks /Change /TN "Microsoft\Windows\Maintenance\WinSAT" /disable
          schtasks /Change /TN "Microsoft\Windows\PI\Sqm-Tasks" /disable
          schtasks /Change /TN "Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyMonitor" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyRefresh" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyUpload" /disable
          schtasks /Change /TN "Microsoft\Windows\Windows Error Reporting\QueueReporting" /disable
          schtasks /Change /TN "Microsoft\Windows\WindowsUpdate\Automatic App Update" /disable
          schtasks /Change /TN "Microsoft\Windows\License Manager\TempSignedLicenseExchange" /disable
          schtasks /Change /TN "Microsoft\Windows\Clip\License Validation" /disable
          schtasks /Change /TN "\Microsoft\Windows\ApplicationData\DsSvcCleanup" /disable
          schtasks /Change /TN "\Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /disable
          schtasks /Change /TN "\Microsoft\Windows\PushToInstall\LoginCheck" /disable
          schtasks /Change /TN "\Microsoft\Windows\PushToInstall\Registration" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyMonitor" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyMonitorToastTask" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyRefreshTask" /disable
          schtasks /Change /TN "\Microsoft\Windows\Subscription\EnableLicenseAcquisition" /disable
          schtasks /Change /TN "\Microsoft\Windows\Subscription\LicenseAcquisition" /disable
          schtasks /Change /TN "\Microsoft\Windows\Diagnosis\RecommendedTroubleshootingScanner" /disable
          schtasks /Change /TN "\Microsoft\Windows\Diagnosis\Scheduled" /disable
          schtasks /Change /TN "\Microsoft\Windows\NetTrace\GatherNetworkInfo" /disable
          del /F /Q "C:\Windows\System32\Tasks\Microsoft\Windows\SettingSync\*"
        echo Done!
        cls
        echo Finally, go to https://github.com/adolfintel/Windows10-Privacy again and follow the steps for the last touches and you are done!
        pause
        del "%CD%\iteration1part1.txt"
        del "%CD%\iteration1part2.txt"
        del "%CD%\iteration2.txt"
        del "%CD%\iteration3.txt"
        del "%USERPROFILE%\Desktop\Manage"
        del "%USERPROFILE%\Desktop\Gaming"
        del "%CD%\Ignite with bite.bat"
        goto end
      )
    )
  )
)
:end
