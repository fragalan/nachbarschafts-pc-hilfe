@echo off
color a
setlocal EnableDelayedExpansion
set /p in=Do you really want to execute the script? [y/n]
if "!in!"=="y" goto execscript
if "!in!"=="j" goto execscript
if "!in!"=="" goto execscript
goto end
:execscript
cls
if NOT EXIST "%CD%\iteration1part1.txt" (
  echo. > "%CD%\iteration1part1.txt"
  echo First remove everything you can!
  pause
  cls
  echo Getting install_wim_tweak...
    PowerShell -Command "& {Invoke-WebRequest https://raw.githubusercontent.com/adolfintel/Windows10-Privacy/master/data/install_wim_tweak.zip -OutFile '%CD%\install_wim_tweak.zip'}"
    PowerShell -Command "& {Expand-Archive -Force '%CD%\install_wim_tweak.zip' 'C:\Windows\System32'}"
    del "%CD%\install_wim_tweak.zip"
  echo Go to C:\Windows\System32, check for install_wim_tweak and continue
  pause
  cls
  echo Getting the Ninite Apps...
    PowerShell -Command "& {Invoke-WebRequest https://ninite.com/firefox-irfanview-klitecodecs-sumatrapdf-winrar/ninite.exe -OutFile '%CD%\Ninite.exe'}"
    "%CD%\Ninite.exe"
  pause
    del "%CD%\Ninite.exe"
  cls
  set /p WD=Do you want to remove the Windows Defender? [y/n]
  if "!WD!"=="y" goto WD
  if "!WD!"=="j" goto WD
  if "!WD!"=="" goto WD
  goto nWD
  :WD
  echo Removing Windows Defender...
    reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v SmartScreenEnabled /t REG_SZ /d "Off" /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d "0" /f
    reg add "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d "0" /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender" /v DisableAntiSpyware /t REG_DWORD /d 1 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v SpyNetReporting /t REG_DWORD /d 0 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v SubmitSamplesConsent /t REG_DWORD /d 2 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v DontReportInfectionInformation /t REG_DWORD /d 1 /f
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\Sense" /f
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\SecurityHealthService" /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontReportInfectionInformation" /t REG_DWORD /d 1 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontOfferThroughWUAU" /t REG_DWORD /d 1 /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "SecurityHealth" /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run" /v "SecurityHealth" /f
    reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\SecHealthUI.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
    install_wim_tweak /o /c Windows-Defender /r
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Notifications\Settings\Windows.SystemToast.SecurityAndMaintenance" /v "Enabled" /t REG_DWORD /d 0 /f
  echo Done!
  :nWD
  pause
  cls
  set /p WS=Do you want to remove the Windows Store? [y/n]
  if "!WS!"=="y" goto WS
  if "!WS!"=="j" goto WS
  if "!WS!"=="" goto WS
  goto nWS
  :WS
  echo Removing Windows Store...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *store* | Remove-AppxPackage}"
    install_wim_tweak /o /c Microsoft-Windows-ContentDeliveryManager /r
    install_wim_tweak /o /c Microsoft-Windows-Store /r
    reg add "HKLM\Software\Policies\Microsoft\WindowsStore" /v RemoveWindowsStore /t REG_DWORD /d 1 /f
    reg add "HKLM\Software\Policies\Microsoft\WindowsStore" /v DisableStoreApps /t REG_DWORD /d 1 /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d 0 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\PushToInstall" /v DisablePushToInstall /t REG_DWORD /d 1 /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v SilentInstalledAppsEnabled /t REG_DWORD /d 0 /f
    sc delete PushToInstall
  echo Done!
  :nWS
  pause
  cls
  set /p MTV=Do you want to remove the Windows Apps for Music, TV, etc.? [y/n]
  if "!MTV!"=="y" goto MTV
  if "!MTV!"=="j" goto MTV
  if "!MTV!"=="" goto MTV
  goto nMTV
  :MTV
  echo Removing Music, TV, etc. ...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *zune* | Remove-AppxPackage}"
    PowerShell -Command "& {Get-WindowsPackage -Online | Where PackageName -like *MediaPlayer* | Remove-WindowsPackage -Online -NoRestart}"
  echo Done!
  :nMTV
  pause
  cls
  set /p XBOX=Do you want to remove the Windows Apps XBox and Game DVR? [y/n]
  if "!XBOX!"=="y" goto XBOX
  if "!XBOX!"=="j" goto XBOX
  if "!XBOX!"=="" goto XBOX
  goto nXBOX
  :XBOX
  echo Removing XBox and Game DVR...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *xbox* | Remove-AppxPackage}"
    sc delete XblAuthManager
    sc delete XblGameSave
    sc delete XboxNetApiSvc
    sc delete XboxGipSvc
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\xbgm" /f
    schtasks /Change /TN "Microsoft\XblGameSave\XblGameSaveTask" /disable
    schtasks /Change /TN "Microsoft\XblGameSave\XblGameSaveTaskLogon" /disable
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\GameDVR" /v AllowGameDVR /t REG_DWORD /d 0 /f
  echo Done!
  echo Additionally, go to Start > Settings > Gaming and turn off everything!
  :nXBOX
  pause
  cls
  set /p SN=Do you want to remove the Windows App Sticky Notes? [y/n]
  if "!SN!"=="y" goto SN
  if "!SN!"=="j" goto SN
  if "!SN!"=="" goto SN
  goto nSN
  :SN
  echo Removing Sticky Notes...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *sticky* | Remove-AppxPackage}"
  echo Done!
  :nSN
  pause
  cls
  set /p MAPS=Do you want to remove the Windows App for Maps? [y/n]
  if "!MAPS!"=="y" goto MAPS
  if "!MAPS!"=="j" goto MAPS
  if "!MAPS!"=="" goto MAPS
  goto nMAPS
  :MAPS
  echo Removing Maps...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *maps* | Remove-AppxPackage}"
    sc delete MapsBroker
    sc delete lfsvc
    schtasks /Change /TN "\Microsoft\Windows\Maps\MapsUpdateTask" /disable
    schtasks /Change /TN "\Microsoft\Windows\Maps\MapsToastTask" /disable
  echo Done!
  :nMAPS
  pause
  cls
  set /p AC=Do you want to remove the Windows Apps Alarms and Clock? [y/n]
  if "!AC!"=="y" goto AC
  if "!AC!"=="j" goto AC
  if "!AC!"=="" goto AC
  goto nAC
  :AC
  echo Removing Alarms and Clock...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *alarms* | Remove-AppxPackage}"
    PowerShell -Command "& {Get-AppxPackage -AllUsers *people* | Remove-AppxPackage}"
  echo Done!
  :nAC
  pause
  cls
  set /p MC=Do you want to remove the Windows Apps Mail, Calendar, etc.? [y/n]
  if "!MC!"=="y" goto MC
  if "!MC!"=="j" goto MC
  if "!MC!"=="" goto MC
  goto nMC
  :MC
  echo Removing Mail, Calendar, etc. ...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *comm* | Remove-AppxPackage}"
    PowerShell -Command "& {Get-AppxPackage -AllUsers *mess* | Remove-AppxPackage}"
  echo Done!
  :nMC
  pause
  cls
  set /p ON=Do you want to remove the Windows App OneNote? [y/n]
  if "!ON!"=="y" goto ON
  if "!ON!"=="j" goto ON
  if "!ON!"=="" goto ON
  goto nON
  :ON
  echo Removing OneNote...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *onenote* | Remove-AppxPackage}"
  echo Done!
  :nON
  pause
  cls
  set /p PH=Do you want to remove the Windows App Photos? [y/n]
  if "!PH!"=="y" goto PH
  if "!PH!"=="j" goto PH
  if "!PH!"=="" goto PH
  goto nPH
  :PH
  echo Removing Photos...
    PowerShell -Command "& {Get-AppxPackage -AllUsers *photo* | Remove-AppxPackage}"
  echo Done!
  :nPH
  pause
  cls
)
if EXIST "%CD%\iteration1part1.txt" (
  if NOT EXIST "%CD%\iteration1part2.txt" (
    echo. > "%CD%\iteration1part2.txt"
    set /p CA=Do you want to remove the Windows App Camera? [y/n]
    if "!CA!"=="y" goto CA
    if "!CA!"=="j" goto CA
    if "!CA!"=="" goto CA
    goto nCA
    :CA
    echo Removing Camera...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *camera* | Remove-AppxPackage}"
    echo Done!
    :nCA
    pause
    cls
    set /p WN=Do you want to remove the Windows Apps Weather, News, etc.? [y/n]
    if "!WN!"=="y" goto WN
    if "!WN!"=="j" goto WN
    if "!WN!"=="" goto WN
    goto nWN
    :WN
    echo Removing Weather, News, etc. ...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *bing* | Remove-AppxPackage}"
    echo Done!
    :nWN
    pause
    cls
    set /p CALC=Do you want to remove the Windows App Calculator? [y/n]
    if "!CALC!"=="y" goto CALC
    if "!CALC!"=="j" goto CALC
    if "!CALC!"=="" goto CALC
    goto nCALC
    :CALC
    echo Removing Calculator...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *calc* | Remove-AppxPackage}"
    echo Done!
    :nCALC
    pause
    cls
    set /p SR=Do you want to remove the Windows App Sound Recorder? [y/n]
    if "!SR!"=="y" goto SR
    if "!SR!"=="j" goto SR
    if "!SR!"=="" goto SR
    goto nSR
    :SR
    echo Removing Sound Recorder...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *soundrec* | Remove-AppxPackage}"
    echo Done!
    :nSR
    pause
    cls
    set /p CG=Do you want to remove the Windows Apps Contact Support and Get Help? [y/n]
    if "!CG!"=="y" goto CG
    if "!CG!"=="j" goto CG
    if "!CG!"=="" goto CG
    goto nCG
    :CG
    echo Removing Contact Support and Get Help
      install_wim_tweak /o /c Microsoft-Windows-ContactSupport /r
      PowerShell -Command "& {Get-AppxPackage -AllUsers *GetHelp* | Remove-AppxPackage}"
    echo Additionally, go to Start > Settings > Apps > Manage optional features, and remove Contact Support, if present!
    :nCG
    pause
    cls
    set /p MQA=Do you want to remove the Windows App Microsoft Quick Assist? [y/n]
    if "!MQA!"=="y" goto MQA
    if "!MQA!"=="j" goto MQA
    if "!MQA!"=="" goto MQA
    goto nMQA
    :MQA
    echo Removing Microsoft Quick Assist...
      PowerShell -Command "& {Get-WindowsPackage -Online | Where PackageName -like *QuickAssist* | Remove-WindowsPackage -Online -NoRestart}"
    echo Done!
    :nMQA
    pause
    cls
    set /p C=Do you want to remove the Windows App Connect? [y/n]
    if "!C!"=="y" goto C
    if "!C!"=="j" goto C
    if "!C!"=="" goto C
    goto nC
    :C
    echo Removing Connect...
      install_wim_tweak /o /c Microsoft-PPIProjection-Package /r
    echo Done!
    :nC
    pause
    cls
    set /p YP=Do you want to remove the Windows App Your Phone? [y/n]
    if "!YP!"=="y" goto YP
    if "!YP!"=="j" goto YP
    if "!YP!"=="" goto YP
    goto nYP
    :YP
    echo Removing Your Phone...
      PowerShell -Command "& {Get-AppxPackage -AllUsers *phone* | Remove-AppxPackage}"
    echo Done!
    :nYP
    pause
    cls
    set /p HF=Do you want to remove the Windows App Hello Face? [y/n]
    if "!HF!"=="y" goto HF
    if "!HF!"=="j" goto HF
    if "!HF!"=="" goto HF
    goto nHF
    :HF
    echo Removing Hello Face...
      PowerShell -Command "& {Get-WindowsPackage -Online | Where PackageName -like *Hello-Face* | Remove-WindowsPackage -Online -NoRestart}"
      schtasks /Change /TN "\Microsoft\Windows\HelloFace\FODCleanupTask" /Disable
    echo Done!
    :nHF
    pause
    cls
    set /p PP=Do you want to remove the Windows App Paint 3D and Print 3D? [y/n]
    if "!PP!"=="y" goto PP
    if "!PP!"=="j" goto PP
    if "!PP!"=="" goto PP
    goto nPP
    :PP
    echo Removing Paint 3D and Print 3D...
      for /f "tokens=1* delims=" %%I in (' reg query "HKEY_CLASSES_ROOT\SystemFileAssociations" /s /k /f "3D Edit" ^| find /i "3D Edit" ') do (reg delete "%%I" /f )
      for /f "tokens=1* delims=" %%I in (' reg query "HKEY_CLASSES_ROOT\SystemFileAssociations" /s /k /f "3D Print" ^| find /i "3D Print" ') do (reg delete "%%I" /f )
    echo Done!
    :nPP
    pause
    cls
    set /p SYR=Do you want to remove the Windows Feature to restore your system? [y/n]
    if "!SYR!"=="y" goto SYR
    if "!SYR!"=="j" goto SYR
    if "!SYR!"=="" goto SYR
    goto nSYR
    :SYR
    echo Removing System Restore...
      PowerShell -Command "& {Disable-ComputerRestore -Drive 'C:\'}"
      PowerShell -Command "& {vssadmin delete shadows /all /Quiet}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore' /v 'DisableConfig' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore' /v 'DisableSR ' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore' /v 'DisableConfig' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore' /v 'DisableSR ' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {schtasks /Change /TN '\Microsoft\Windows\SystemRestore\SR' /disable}"
    echo Done!
    :nSYR
    pause
    cls
    echo Your system will reboot now!
    shutdown -r -t 1
    goto end
  )
)
if EXIST "%CD%\iteration1part1.txt" (
  if EXIST "%CD%\iteration1part2.txt" (
    if NOT EXIST "%CD%\iteration2.txt" (
      echo. > "%CD%\iteration2.txt"
      set /p CO=Do you want to disable Cortana? [y/n]
      if "!CO!"=="y" goto CO
      if "!CO!"=="j" goto CO
      if "!CO!"=="" goto CO
      goto nCO
      :CO
      echo Disabling Cortana...
        reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v AllowCortana /t REG_DWORD /d 0 /f
        reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules"  /v "{2765E0F4-2918-4A46-B9C9-43CDD8FCBA2B}" /t REG_SZ /d  "BlockCortana|Action=Block|Active=TRUE|Dir=Out|App=C:\windows\systemapps\microsoft.windows.cortana_cw5n1h2txyewy\searchui.exe|Name=Search  and Cortana  application|AppPkgId=S-1-15-2-1861897761-1695161497-2927542615-642690995-327840285-2659745135-2630312742|" /f
      echo Done!
      :nCO
      pause
      cls
      echo Your system will reboot now!
      shutdown -r -t 1
      goto end
    )
  )
)
if EXIST "%CD%\iteration1part1.txt" (
  if EXIST "%CD%\iteration1part2.txt" (
    if EXIST "%CD%\iteration2.txt" (
      if NOT EXIST "%CD%\iteration3.txt" (
        echo. > "%CD%\iteration3.txt"
        set /p WER=Do you want to disable the reporting of errors? [y/n]
        if "!WER!"=="y" goto WER
        if "!WER!"=="j" goto WER
        if "!WER!"=="" goto WER
        goto nWER
        :WER
        echo Disabling Windows Error Report...
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f
        echo Done!
        :nWER
        pause
        cls
        set /p FU=Do you want to disable forced updates? [y/n]
        if "!FU!"=="y" goto FU
        if "!FU!"=="j" goto FU
        if "!FU!"=="" goto FU
        goto nFU
        :FU
        echo Disabling Forced Updates...
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v NoAutoUpdate /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v AUOptions /t REG_DWORD /d 2 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v ScheduledInstallDay /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v ScheduledInstallTime /t REG_DWORD /d 3 /f
        echo Done!
        :nFU
        pause
        cls
        set /p LC=Do you want to disable the checking for a Windows License? [y/n]
        if "!LC!"=="y" goto LC
        if "!LC!"=="j" goto LC
        if "!LC!"=="" goto LC
        goto nLC
        :LC
        echo Disabling License Checking...
          reg add "HKLM\Software\Policies\Microsoft\Windows NT\CurrentVersion\Software Protection Platform" /v NoGenTicket /t REG_DWORD /d 1 /f
        echo Done!
        :nLC
        pause
        cls
        set /p S=Do you want to disable Sync? [y/n]
        if "!S!"=="y" goto S
        if "!S!"=="j" goto S
        if "!S!"=="" goto S
        goto nS
        :S
        echo Diabling Sync...
          reg add "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v DisableSettingSync /t REG_DWORD /d 2 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v DisableSettingSyncUserOverride /t REG_DWORD /d 1 /f
        echo Done!
        :nS
        pause
        cls
        set /p WT=Do you want to disable the Windows Tips? [y/n]
        if "!WT!"=="y" goto WT
        if "!WT!"=="j" goto WT
        if "!WT!"=="" goto WT
        goto nWT
        :WT
        echo Disabling Windows Tips...
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableSoftLanding /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsSpotlightFeatures /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsConsumerFeatures /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\DataCollection" /v DoNotShowFeedbackNotifications /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\WindowsInkWorkspace" /v AllowSuggestedAppsInWindowsInkWorkspace /t REG_DWORD /d 0 /f
        echo Done!
        :nWT
        pause
        cls
        set /p TS=Do you want to remove Telemetry and other Unnecessary Services? [y/n]
        if "!TS!"=="y" goto TS
        if "!TS!"=="j" goto TS
        if "!TS!"=="" goto TS
        goto nTS
        :TS
        echo Removing Telemetry and other Unnecessary Services...
          sc delete DiagTrack
          sc delete dmwappushservice
          sc delete WerSvc
          sc delete OneSyncSvc
          sc delete MessagingService
          sc delete wercplsupport
          sc delete PcaSvc
          sc config wlidsvc start=demand
          sc delete wisvc
          sc delete RetailDemo
          sc delete diagsvc
          sc delete shpamsvc 
          sc delete TermService
          sc delete UmRdpService
          sc delete SessionEnv
          sc delete TroubleshootingSvc
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "wscsvc" ^| find /i "wscsvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "OneSyncSvc" ^| find /i "OneSyncSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "MessagingService" ^| find /i "MessagingService"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "PimIndexMaintenanceSvc" ^| find /i "PimIndexMaintenanceSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "UserDataSvc" ^| find /i "UserDataSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "UnistoreSvc" ^| find /i "UnistoreSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "BcastDVRUserService" ^| find /i "BcastDVRUserService"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "Sgrmbroker" ^| find /i "Sgrmbroker"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "ClipSVC" ^| find /i "ClipSVC"') do (reg delete %%I /f)
          sc delete diagnosticshub.standardcollector.service
          reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules" /v "NumberOfSIUFInPeriod" /t REG_DWORD /d 0 /f
          reg delete "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules" /v "PeriodInNanoSeconds" /f
          reg add "HKLM\SYSTEM\ControlSet001\Control\WMI\AutoLogger\AutoLogger-Diagtrack-Listener" /v Start /t REG_DWORD /d 0 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v AITEnable /t REG_DWORD /d 0 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisableInventory /t REG_DWORD /d 1 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisablePCA /t REG_DWORD /d 1 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisableUAR /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableSmartScreen" /t REG_DWORD /d 0 /f
          reg add "HKCU\Software\Microsoft\Internet Explorer\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d 0 /f
          reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoRecentDocsHistory" /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\CompatTelRunner.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\DeviceCensus.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
        echo Done!
        :nTS
        pause
        cls
        start firefox.exe -private-window https://github.com/adolfintel/Windows10-Privacy
        echo Go to https://github.com/adolfintel/Windows10-Privacy and follow the steps to delete the keys DPS, WdiServiceHost and WdiServiceHost from the registry!
        pause
        cls
        set /p ST=Do you want to disable the Scheduld Tasks of Windows? [y/n]
        if "!ST!"=="y" goto ST
        if "!ST!"=="j" goto ST
        if "!ST!"=="" goto ST
        goto nST
        :ST
        echo Disabling Scheduled Tasks...
          schtasks /Change /TN "Microsoft\Windows\AppID\SmartScreenSpecific" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\AitAgent" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\ProgramDataUpdater" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\StartupAppTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Autochk\Proxy" /disable
          schtasks /Change /TN "Microsoft\Windows\CloudExperienceHost\CreateObjectTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\BthSQM" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Uploader" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /disable
          schtasks /Change /TN "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" /disable
          schtasks /Change /TN "Microsoft\Windows\DiskFootprint\Diagnostics" /disable
          schtasks /Change /TN "Microsoft\Windows\FileHistory\File History (maintenance mode)" /disable
          schtasks /Change /TN "Microsoft\Windows\Maintenance\WinSAT" /disable
          schtasks /Change /TN "Microsoft\Windows\PI\Sqm-Tasks" /disable
          schtasks /Change /TN "Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyMonitor" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyRefresh" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyUpload" /disable
          schtasks /Change /TN "Microsoft\Windows\Windows Error Reporting\QueueReporting" /disable
          schtasks /Change /TN "Microsoft\Windows\WindowsUpdate\Automatic App Update" /disable
          schtasks /Change /TN "Microsoft\Windows\License Manager\TempSignedLicenseExchange" /disable
          schtasks /Change /TN "Microsoft\Windows\Clip\License Validation" /disable
          schtasks /Change /TN "\Microsoft\Windows\ApplicationData\DsSvcCleanup" /disable
          schtasks /Change /TN "\Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /disable
          schtasks /Change /TN "\Microsoft\Windows\PushToInstall\LoginCheck" /disable
          schtasks /Change /TN "\Microsoft\Windows\PushToInstall\Registration" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyMonitor" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyMonitorToastTask" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyRefreshTask" /disable
          schtasks /Change /TN "\Microsoft\Windows\Subscription\EnableLicenseAcquisition" /disable
          schtasks /Change /TN "\Microsoft\Windows\Subscription\LicenseAcquisition" /disable
          schtasks /Change /TN "\Microsoft\Windows\Diagnosis\RecommendedTroubleshootingScanner" /disable
          schtasks /Change /TN "\Microsoft\Windows\Diagnosis\Scheduled" /disable
          schtasks /Change /TN "\Microsoft\Windows\NetTrace\GatherNetworkInfo" /disable
          del /F /Q "C:\Windows\System32\Tasks\Microsoft\Windows\SettingSync\*"
        echo Done!
        :nST
        pause
        cls
        echo Finally, go to https://github.com/adolfintel/Windows10-Privacy again and follow the steps for the last touches and you are done!
        pause
        del "%CD%\iteration1part1.txt"
        del "%CD%\iteration1part2.txt"
        del "%CD%\iteration2.txt"
        del "%CD%\iteration3.txt"
        del "%USERPROFILE%\Desktop\Manage"
        del "%USERPROFILE%\Desktop\Gaming"
        del "%CD%\Ignite.bat"
        goto end
      )
    )
  )
)
:end
