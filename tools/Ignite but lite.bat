@echo off
color a
setlocal EnableDelayedExpansion
set /p in=Do you really want to execute the script? [y/n]
if "!in!"=="y" goto execscript
if "!in!"=="j" goto execscript
if "!in!"=="" goto execscript
goto end
:execscript
cls
if NOT EXIST "%CD%\iteration1part1.txt" (
  echo. > "%CD%\iteration1part1.txt"
  echo First remove everything you can!
  pause
  cls
  echo Getting install_wim_tweak...
    PowerShell -Command "& {Invoke-WebRequest https://raw.githubusercontent.com/adolfintel/Windows10-Privacy/master/data/install_wim_tweak.zip -OutFile '%CD%\install_wim_tweak.zip'}"
    PowerShell -Command "& {Expand-Archive -Force '%CD%\install_wim_tweak.zip' 'C:\Windows\System32'}"
    del "%CD%\install_wim_tweak.zip"
  echo Go to C:\Windows\System32, check for install_wim_tweak and continue
  pause
  cls
  echo Getting the latest Firefox...
    PowerShell -Command "& {Invoke-WebRequest https://ninite.com/firefox/ninite.exe -OutFile '%CD%\Ninite.exe'}"
    "%CD%\Ninite.exe"
  pause
    del "%CD%\Ninite.exe"
  cls
  set /p WD=Do you want to remove the Windows Defender? [y/n]
  if "!WD!"=="y" goto WD
  if "!WD!"=="j" goto WD
  if "!WD!"=="" goto WD
  goto nWD
  :WD
  echo Removing Windows Defender...
    reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v SmartScreenEnabled /t REG_SZ /d "Off" /f
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d "0" /f
    reg add "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d "0" /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender" /v DisableAntiSpyware /t REG_DWORD /d 1 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v SpyNetReporting /t REG_DWORD /d 0 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v SubmitSamplesConsent /t REG_DWORD /d 2 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v DontReportInfectionInformation /t REG_DWORD /d 1 /f
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\Sense" /f
    reg delete "HKLM\SYSTEM\CurrentControlSet\Services\SecurityHealthService" /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontReportInfectionInformation" /t REG_DWORD /d 1 /f
    reg add "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontOfferThroughWUAU" /t REG_DWORD /d 1 /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "SecurityHealth" /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run" /v "SecurityHealth" /f
    reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\SecHealthUI.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
    install_wim_tweak /o /c Windows-Defender /r
    reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Notifications\Settings\Windows.SystemToast.SecurityAndMaintenance" /v "Enabled" /t REG_DWORD /d 0 /f
  echo Done!
  :nWD
  pause
  cls
)
if EXIST "%CD%\iteration1part1.txt" (
  if NOT EXIST "%CD%\iteration1part2.txt" (
    echo. > "%CD%\iteration1part2.txt"
    set /p SYR=Do you want to remove the Windows Feature to restore your system? [y/n]
    if "!SYR!"=="y" goto SYR
    if "!SYR!"=="j" goto SYR
    if "!SYR!"=="" goto SYR
    goto nSYR
    :SYR
    echo Removing System Restore...
      PowerShell -Command "& {Disable-ComputerRestore -Drive 'C:\'}"
      PowerShell -Command "& {vssadmin delete shadows /all /Quiet}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore' /v 'DisableConfig' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore' /v 'DisableSR ' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore' /v 'DisableConfig' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {reg add 'HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore' /v 'DisableSR ' /t 'REG_DWORD' /d '1' /f}"
      PowerShell -Command "& {schtasks /Change /TN '\Microsoft\Windows\SystemRestore\SR' /disable}"
    echo Done!
    :nSYR
    pause
    cls
    echo Your system will reboot now!
    shutdown -r -t 1
    goto end
  )
)
if EXIST "%CD%\iteration1part1.txt" (
  if EXIST "%CD%\iteration1part2.txt" (
    if NOT EXIST "%CD%\iteration2.txt" (
      echo. > "%CD%\iteration2.txt"
      set /p CO=Do you want to disable Cortana? [y/n]
      if "!CO!"=="y" goto CO
      if "!CO!"=="j" goto CO
      if "!CO!"=="" goto CO
      goto nCO
      :CO
      echo Disabling Cortana...
        reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v AllowCortana /t REG_DWORD /d 0 /f
        reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules"  /v "{2765E0F4-2918-4A46-B9C9-43CDD8FCBA2B}" /t REG_SZ /d  "BlockCortana|Action=Block|Active=TRUE|Dir=Out|App=C:\windows\systemapps\microsoft.windows.cortana_cw5n1h2txyewy\searchui.exe|Name=Search  and Cortana  application|AppPkgId=S-1-15-2-1861897761-1695161497-2927542615-642690995-327840285-2659745135-2630312742|" /f
      echo Done!
      :nCO
      pause
      cls
      echo Your system will reboot now!
      shutdown -r -t 1
      goto end
    )
  )
)
if EXIST "%CD%\iteration1part1.txt" (
  if EXIST "%CD%\iteration1part2.txt" (
    if EXIST "%CD%\iteration2.txt" (
      if NOT EXIST "%CD%\iteration3.txt" (
        echo. > "%CD%\iteration3.txt"
        set /p WER=Do you want to disable the reporting of errors? [y/n]
        if "!WER!"=="y" goto WER
        if "!WER!"=="j" goto WER
        if "!WER!"=="" goto WER
        goto nWER
        :WER
        echo Disabling Windows Error Report...
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f
        echo Done!
        :nWER
        pause
        cls
        set /p FU=Do you want to disable forced updates? [y/n]
        if "!FU!"=="y" goto FU
        if "!FU!"=="j" goto FU
        if "!FU!"=="" goto FU
        goto nFU
        :FU
        echo Disabling Forced Updates...
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v NoAutoUpdate /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v AUOptions /t REG_DWORD /d 2 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v ScheduledInstallDay /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v ScheduledInstallTime /t REG_DWORD /d 3 /f
        echo Done!
        :nFU
        pause
        cls
        set /p LC=Do you want to disable the checking for a Windows License? [y/n]
        if "!LC!"=="y" goto LC
        if "!LC!"=="j" goto LC
        if "!LC!"=="" goto LC
        goto nLC
        :LC
        echo Disabling License Checking...
          reg add "HKLM\Software\Policies\Microsoft\Windows NT\CurrentVersion\Software Protection Platform" /v NoGenTicket /t REG_DWORD /d 1 /f
        echo Done!
        :nLC
        pause
        cls
        set /p S=Do you want to disable Sync? [y/n]
        if "!S!"=="y" goto S
        if "!S!"=="j" goto S
        if "!S!"=="" goto S
        goto nS
        :S
        echo Diabling Sync...
          reg add "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v DisableSettingSync /t REG_DWORD /d 2 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v DisableSettingSyncUserOverride /t REG_DWORD /d 1 /f
        echo Done!
        :nS
        pause
        cls
        set /p WT=Do you want to disable the Windows Tips? [y/n]
        if "!WT!"=="y" goto WT
        if "!WT!"=="j" goto WT
        if "!WT!"=="" goto WT
        goto nWT
        :WT
        echo Disabling Windows Tips...
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableSoftLanding /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsSpotlightFeatures /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsConsumerFeatures /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\Windows\DataCollection" /v DoNotShowFeedbackNotifications /t REG_DWORD /d 1 /f
          reg add "HKLM\Software\Policies\Microsoft\WindowsInkWorkspace" /v AllowSuggestedAppsInWindowsInkWorkspace /t REG_DWORD /d 0 /f
        echo Done!
        :nWT
        pause
        cls
        set /p TS=Do you want to remove Telemetry and other Unnecessary Services? [y/n]
        if "!TS!"=="y" goto TS
        if "!TS!"=="j" goto TS
        if "!TS!"=="" goto TS
        goto nTS
        :TS
        echo Removing Telemetry and other Unnecessary Services...
          sc delete DiagTrack
          sc delete dmwappushservice
          sc delete WerSvc
          sc delete OneSyncSvc
          sc delete MessagingService
          sc delete wercplsupport
          sc delete PcaSvc
          sc config wlidsvc start=demand
          sc delete wisvc
          sc delete RetailDemo
          sc delete diagsvc
          sc delete shpamsvc 
          sc delete TermService
          sc delete UmRdpService
          sc delete SessionEnv
          sc delete TroubleshootingSvc
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "wscsvc" ^| find /i "wscsvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "OneSyncSvc" ^| find /i "OneSyncSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "MessagingService" ^| find /i "MessagingService"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "PimIndexMaintenanceSvc" ^| find /i "PimIndexMaintenanceSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "UserDataSvc" ^| find /i "UserDataSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "UnistoreSvc" ^| find /i "UnistoreSvc"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "BcastDVRUserService" ^| find /i "BcastDVRUserService"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "Sgrmbroker" ^| find /i "Sgrmbroker"') do (reg delete %%I /f)
          for /f "tokens=1" %%I in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services" /k /f "ClipSVC" ^| find /i "ClipSVC"') do (reg delete %%I /f)
          sc delete diagnosticshub.standardcollector.service
          reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules" /v "NumberOfSIUFInPeriod" /t REG_DWORD /d 0 /f
          reg delete "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules" /v "PeriodInNanoSeconds" /f
          reg add "HKLM\SYSTEM\ControlSet001\Control\WMI\AutoLogger\AutoLogger-Diagtrack-Listener" /v Start /t REG_DWORD /d 0 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v AITEnable /t REG_DWORD /d 0 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisableInventory /t REG_DWORD /d 1 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisablePCA /t REG_DWORD /d 1 /f
          reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v DisableUAR /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d 0 /f
          reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableSmartScreen" /t REG_DWORD /d 0 /f
          reg add "HKCU\Software\Microsoft\Internet Explorer\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d 0 /f
          reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoRecentDocsHistory" /t REG_DWORD /d 1 /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\CompatTelRunner.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
          reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\DeviceCensus.exe" /v Debugger /t REG_SZ /d "%windir%\System32\taskkill.exe" /f
        echo Done!
        :nTS
        pause
        cls
        start firefox.exe -private-window https://github.com/adolfintel/Windows10-Privacy
        echo Go to https://github.com/adolfintel/Windows10-Privacy and follow the steps to delete the keys DPS, WdiServiceHost and WdiServiceHost from the registry!
        pause
        cls
        set /p ST=Do you want to disable the Scheduld Tasks of Windows? [y/n]
        if "!ST!"=="y" goto ST
        if "!ST!"=="j" goto ST
        if "!ST!"=="" goto ST
        goto nST
        :ST
        echo Disabling Scheduled Tasks...
          schtasks /Change /TN "Microsoft\Windows\AppID\SmartScreenSpecific" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\AitAgent" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\ProgramDataUpdater" /disable
          schtasks /Change /TN "Microsoft\Windows\Application Experience\StartupAppTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Autochk\Proxy" /disable
          schtasks /Change /TN "Microsoft\Windows\CloudExperienceHost\CreateObjectTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\BthSQM" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Uploader" /disable
          schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /disable
          schtasks /Change /TN "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" /disable
          schtasks /Change /TN "Microsoft\Windows\DiskFootprint\Diagnostics" /disable
          schtasks /Change /TN "Microsoft\Windows\FileHistory\File History (maintenance mode)" /disable
          schtasks /Change /TN "Microsoft\Windows\Maintenance\WinSAT" /disable
          schtasks /Change /TN "Microsoft\Windows\PI\Sqm-Tasks" /disable
          schtasks /Change /TN "Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyMonitor" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyRefresh" /disable
          schtasks /Change /TN "Microsoft\Windows\Shell\FamilySafetyUpload" /disable
          schtasks /Change /TN "Microsoft\Windows\Windows Error Reporting\QueueReporting" /disable
          schtasks /Change /TN "Microsoft\Windows\WindowsUpdate\Automatic App Update" /disable
          schtasks /Change /TN "Microsoft\Windows\License Manager\TempSignedLicenseExchange" /disable
          schtasks /Change /TN "Microsoft\Windows\Clip\License Validation" /disable
          schtasks /Change /TN "\Microsoft\Windows\ApplicationData\DsSvcCleanup" /disable
          schtasks /Change /TN "\Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /disable
          schtasks /Change /TN "\Microsoft\Windows\PushToInstall\LoginCheck" /disable
          schtasks /Change /TN "\Microsoft\Windows\PushToInstall\Registration" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyMonitor" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyMonitorToastTask" /disable
          schtasks /Change /TN "\Microsoft\Windows\Shell\FamilySafetyRefreshTask" /disable
          schtasks /Change /TN "\Microsoft\Windows\Subscription\EnableLicenseAcquisition" /disable
          schtasks /Change /TN "\Microsoft\Windows\Subscription\LicenseAcquisition" /disable
          schtasks /Change /TN "\Microsoft\Windows\Diagnosis\RecommendedTroubleshootingScanner" /disable
          schtasks /Change /TN "\Microsoft\Windows\Diagnosis\Scheduled" /disable
          schtasks /Change /TN "\Microsoft\Windows\NetTrace\GatherNetworkInfo" /disable
          del /F /Q "C:\Windows\System32\Tasks\Microsoft\Windows\SettingSync\*"
        echo Done!
        :nST
        pause
        cls
        echo Finally, go to https://github.com/adolfintel/Windows10-Privacy again and follow the steps for the last touches and you are done!
        pause
        del "%CD%\iteration1part1.txt"
        del "%CD%\iteration1part2.txt"
        del "%CD%\iteration2.txt"
        del "%CD%\iteration3.txt"
        del "%USERPROFILE%\Desktop\Manage"
        del "%USERPROFILE%\Desktop\Gaming"
        del "%CD%\Ignite but lite.bat"
        goto end
      )
    )
  )
)
:end
